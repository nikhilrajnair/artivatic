import { MapsAPILoader } from '@agm/core';
import { Component, ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import { StateService } from '../service/state.service'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  title = 'artivatic';
  latitude: number;
  longitude: number;
  zoom: number;
  address: string;
  private geoCoder;

  @ViewChild('search')
  public searchElementRef: ElementRef;
  public dropdownItem: string;

  constructor(private mapsAPILoader: MapsAPILoader, private StateService: StateService, private ngZone: NgZone) {
    // this.dropdownItem = this.actions[0];

  }

  ngOnInit() {
    this.getTableData();

    this.mapsAPILoader.load().then(() => {
      this.setCurrentLocation();
      this.geoCoder = new google.maps.Geocoder;

      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 12;
        });
      });
    });
  }
  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 8;
        this.getAddress(this.latitude, this.longitude);
      });
    }
  }

  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 12;
          this.address = results[0].formatted_address;
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }

    });
  }

  tableData: any;
  dataSource: any;
  statesData: any;
  citiesData: any = [];
  // constructor(private TableService: TableService) { }



  getTableData() {
    this.StateService.getJSON().subscribe(data => {
      console.log(data);
      this.tableData = data;
      this.statesData = this.tableData.states;
      console.log(this.statesData);
    });
  }


  selectedCountry: String = "--Choose Country--";

  states: Array<any>;

  cities: Array<any>;

  selectedState: string = "Select State";
  selectedDistrict: string = "Select City"
  ChangeStateOrder(state: string) {
    console.log(state)
    this.selectedState = state;
    this.cities = this.statesData.filter(cntry => cntry.state === state);
    if (this.cities.length > 0) {
      this.citiesData = this.cities[0].districts;
      this.dropdownItem = this.citiesData;
      console.log(this.dropdownItem)

    }

  }
  ChangeCityOrder(district) {
    this.selectedDistrict = district;
    // this.dropdownItem = this.selectedDistrict;
    console.log(this.selectedDistrict);
  }


  changeState(state) {
    console.log(state);
    this.cities = this.statesData.filter(cntry => cntry.state === state);
    if (this.cities.length > 0) {
      this.citiesData = this.cities[0].districts;
      this.dropdownItem = this.citiesData;
      console.log(this.dropdownItem)
    }
  }

  changeCity(district) {
    this.selectedDistrict = district;
    // this.dropdownItem = this.selectedDistrict;
    console.log(this.selectedDistrict);
  }

}
